<?php


use \App\Core\Migration;

class DataMigration extends Migration
{
  public function up() {

      $rows = [
          [
            'id'    => 1,
            'name'  => 'DVD-disc',
            'position' => 1
          ],
          [
            'id'    => 2,
            'name'  => 'Book',
            'position' => 2
          ],
          [
            'id'    => 3,
            'name'  => 'Furniture',
            'position' => 3
          ]
      ];
      $this->table('types')->insert($rows)->save();

      $rows = [
          [
            'id'    => 1,
            'name'  => 'Size',
            'position' => 1,
            'regex' => '^[0-9]{1,7}(?:\.[0-9]{1,2})?$',
            'helper' => 'Size in MB (Ex: 1; 1.24; 24.5)',
            'measurement' => 'MB'
          ],
          [
            'id'    => 2,
            'name'  => 'Weight',
            'position' => 2,
            'regex' => '^[0-9]{1,7}(?:\.[0-9]{1,2})?$',
            'helper' => 'Weight in KG (Ex: 1; 1.24; 24.5)',
            'measurement' => 'KG'
          ],
          [
            'id'    => 3,
            'name'  => 'Width',
            'position' => 3,
            'regex' => '^[0-9]{1,7}(?:\.[0-9]{1,2})?$',
            'helper' => 'Width in CM (Ex: 1; 1.24; 24.5)',
            'measurement' => 'CM'
          ],
          [
            'id'    => 4,
            'name'  => 'Height',
            'position' => 4,
            'regex' => '^[0-9]{1,7}(?:\.[0-9]{1,2})?$',
            'helper' => 'Height in CM (Ex: 1; 1.24; 24.5)',
            'measurement' => 'CM'
          ],
          [
            'id'    => 5,
            'name'  => 'Length',
            'position' => 5,
            'regex' => '^[0-9]{1,7}(?:\.[0-9]{1,2})?$',
            'helper' => 'Length in CM (Ex: 1; 1.24; 24.5)',
            'measurement' => 'CM'
          ]
      ];
      $this->table('attributes')->insert($rows)->save();

      $rows = [
          [
            'type_id'    => 1,
            'attribute_id'  => 1,
          ],
          [
            'type_id'    => 2,
            'attribute_id'  => 2,
          ],
          [
            'type_id'    => 3,
            'attribute_id'  => 3,
          ],
          [
            'type_id'    => 3,
            'attribute_id'  => 4,
          ],
          [
            'type_id'    => 3,
            'attribute_id'  => 5,
          ]
      ];
      $this->table('attribute_type')->insert($rows)->save();


  }

  public function down() {
    $this->execute('TRUNCATE types');
    $this->execute('TRUNCATE attributes');
    $this->execute('TRUNCATE attribute_type');
  }
}

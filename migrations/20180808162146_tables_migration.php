<?php

use \App\Core\Migration;

class TablesMigration extends Migration {
    public function up() {
        $this->schema->create('products', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('sku');
            $table->decimal('price');
        });

        $this->schema->create('types', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('position');
        });

        $this->schema->create('attributes', function(Illuminate\Database\Schema\Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('position');
            $table->string('regex');
            $table->string('helper');
            $table->string('measurement');
        });

        $this->schema->create('attribute_type', function(Illuminate\Database\Schema\Blueprint $table){
            $table->integer('attribute_id');
            $table->integer('type_id');
        });

        $this->schema->create('products_attributes_values', function(Illuminate\Database\Schema\Blueprint $table){
            $table->integer('product_id');
            $table->integer('attribute_id');
            $table->string('value');
        });
    }

    public function down() {
        $this->schema->drop('products');
        $this->schema->drop('types');
        $this->schema->drop('attribute_type');
        $this->schema->drop('attributes');
        $this->schema->drop('products_attributes_values');
    }
}

var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var UglifyJsPlugin = require("uglifyjs-webpack-plugin");
var OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
  context: __dirname,
  devtool: false,
  entry: "./assets/js/main.js",
  optimization: {
    minimizer: [
      new UglifyJsPlugin({}),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          "css-loader"
        ]
      }
    ]
  },
  node: {
   fs: "empty"
  },
  output: {
    path: __dirname + "/assets/build",
    filename: "main.min.js"
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].min.css"
    })
  ]
};

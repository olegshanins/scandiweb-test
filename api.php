<?php
/**
 * This file handles some http requests "forwarded" from router.php
 * All static files like png,jpg,jpeg,gif,css,js,html are served to browser NOT through this file.
 * All requests like "/products/all" will be served by this api.php file
 *
 *
 *
 * Autoload all libraries what has been load by composer.
 * Packages from vendors and application itself.
 */
require __DIR__ . '/vendor/autoload.php';

/**
* Initialize object what helps to handle routes on request
*/
$klein = new \Klein\Klein();

/**
 * Handle all routes under /products
 * All routes return json response to browser by controller method
 */
$klein->with('/products', function () use ($klein) {

    /**
     * Get instance of ProductController
     */
    $productCotroller = new App\ProductController;

    /**
     * Get all products with their atttributes
     */
    $klein->respond('GET', '/all', function () use ($productCotroller) {
        $productCotroller->list();
    });

    /**
     * Find product by specific field like id, name, sku, price
     */
    $klein->respond('POST', '/findByField', function () use ($productCotroller) {
        $productCotroller->product();
    });

    /**
     * Removes products by their IDs
     */
    $klein->respond('POST', '/batchDelete', function () use ($productCotroller) {
        $productCotroller->batchDelete();
    });

    /**
     * Adds a product
     */
    $klein->respond('POST', '/add', function () use ($productCotroller) {
        $productCotroller->add();
    });
});

/**
 * Handle all routes under /types
 * All routes return json response to browser by controller method
 */
$klein->with('/types', function () use ($klein) {

    /**
     * Get instance of TypeController
     */
    $typeCotroller = new App\TypeController;

    /**
     * Get all types
     */
    $klein->respond('GET', '/all', function () use ($typeCotroller) {
        $typeCotroller->list();
    });

    /**
     * Get attributs by type ID
     */
    $klein->respond('POST', '/attributesByTypeId', function () use ($typeCotroller) {
        $typeCotroller->attributes();
    });
});

/**
 * Execute matched route and calls appropriate controller
 */
$klein->dispatch();

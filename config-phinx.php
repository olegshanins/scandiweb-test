<?php
return [
  'paths' => [
    'migrations' => 'migrations'
  ],
  'migration_base_class' => '\App\Core\Migration',
  'environments' => [
    'default_migration_table' => 'phinxlog',
    'default_database' => 'dev',
    'dev' => [
      'adapter' => 'sqlite',
      'name' => './db/phpsqlite',
      'suffix' => '.db'
    ]
  ]
];

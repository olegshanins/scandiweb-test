var $ = require("jquery");
var Handlebars = require('handlebars');
require("bootstrap");
require("jquery-validation");

require('bootstrap/dist/css/bootstrap.css');
require("../css/4-col-portfolio.css");



var templateLoader = (function() {
  'use strict';

  // States of loaded templates
  var STATES = {
    unloaded: 0,
    loaded: 1,
    error: -1
  };

  // Registations data store
  var registrations = {};

  // Internal class for storing registrations
  var registration = function(key, path, html) {
    this.key = key;
    this.path = path;
    this.html = html;
    if (html) this.state = STATES.loaded; // mark loaded
    else this.state = STATES.unloaded; // mark unloaded
  }

  // Loads the templates html
  var load = function(reg, async) {
    var me = this;

    // only load if in unloaded state
    if (reg.state == STATES.unloaded) {
      $.ajax({
        url: reg.path,
        async: async,
        success: function(data) {
          reg.html = data;
          reg.state = STATES.loaded;
          return reg.html
        },
        error: function(data) {
          reg.state = STATES.error;
          console.log(data);
        }
      });
    }
  }

  return {
    /** Register a new template
     * @param {object} obj - registration object that contains key and either path or data
     * @param {bool} loadNow - indicates if the template should be syncrhonously now
     */
    register: function(obj, loadSync) {
      // do some validation
      if (!obj.key)
        throw "templates.register requires a key value";
      if (!(obj.path || obj.data))
        throw "templates.register requires a path or data value";

      // do registration
      var reg = new registration(obj.key, obj.path, obj.data);
      registrations[reg.key] = reg;

      // optionally load stuff
      if (loadSync)
        load(reg, false);
      else
        load(reg, true);
    },
    /** Get a template's html
     * @param {string} key - the template to get
     */
    html: function(key) {
      // check for bad states
      if (!registrations[key]) throw "Template has not been registered";
      if (registrations[key].state == STATES.error) throw "Template load failure";
      var reg = registrations[key];

      // load if necessary
      if (reg.state == STATES.unloaded) {
        load(reg, false);
      }
      return reg.html
    }
  }
}());



Handlebars.registerHelper('var', function(name, value, context) {
  this[name] = value;
});

// for detailed comments and demo, see my SO answer here http://stackoverflow.com/questions/8853396/logical-operator-in-a-handlebars-js-if-conditional/21915381#21915381

/* a helper to execute an IF statement with any expression
	USAGE:
 -- Yes you NEED to properly escape the string literals, or just alternate single and double quotes
 -- to access any global function or property you should use window.functionName() instead of just functionName()
 -- this example assumes you passed this context to your handlebars template( {name: 'Sam', age: '20' } ), notice age is a string, just for so I can demo parseInt later
 <p>
	 {{#xif " name == 'Sam' && age === '12' " }}
		 BOOM
	 {{else}}
		 BAMM
	 {{/xif}}
 </p>
 */

Handlebars.registerHelper("xif", function(expression, options) {
  return Handlebars.helpers["x"].apply(this, [expression, options]) ?
    options.fn(this) : options.inverse(this);
});

/* a helper to execute javascript expressions
 USAGE:
 -- Yes you NEED to properly escape the string literals or just alternate single and double quotes
 -- to access any global function or property you should use window.functionName() instead of just functionName(), notice how I had to use window.parseInt() instead of parseInt()
 -- this example assumes you passed this context to your handlebars template( {name: 'Sam', age: '20' } )
 <p>Url: {{x " \"hi\" + name + \", \" + window.location.href + \" <---- this is your href,\" + " your Age is:" + window.parseInt(this.age, 10) "}}</p>
 OUTPUT:
 <p>Url: hi Sam, http://example.com <---- this is your href, your Age is: 20</p>
*/
Handlebars.registerHelper("x", function(expression, options) {
  var result;

  // you can change the context, or merge it with options.data, options.hash
  var context = this;

  // yup, i use 'with' here to expose the context's properties as block variables
  // you don't need to do {{x 'this.age + 2'}}
  // but you can also do {{x 'age + 2'}}
  // HOWEVER including an UNINITIALIZED var in a expression will return undefined as the result.
  with(context) {
    result = (function() {
      try {
        return eval(expression);
      } catch (e) {
        console.warn('�Expression: {{x \'' + expression +
          '\'}}\n�JS-Error: ', e, '\n�Context: ', context);
      }
    }).call(context); // to make eval's lexical this=context
  }
  return result;
});

/*
	if you want access upper level scope, this one is slightly different
	the expression is the JOIN of all arguments
	usage: say context data looks like this:

		// data
		{name: 'Sam', age: '20', address: { city: 'yomomaz' } }

		// in template
		// notice how the expression wrap all the string with quotes, and even the variables
		// as they will become strings by the time they hit the helper
		// play with it, you will immediately see the errored expressions and figure it out

		{{#with address}}
				{{z '"hi " + "' ../this.name '" + " you live with " + "' city '"' }}
			{{/with}}
*/
Handlebars.registerHelper("z", function() {
  var options = arguments[arguments.length - 1]
  delete arguments[arguments.length - 1];
  return Handlebars.helpers["x"].apply(this, [Array.prototype.slice.call(
    arguments, 0).join(''), options]);
});

Handlebars.registerHelper("zif", function() {
  var options = arguments[arguments.length - 1]
  delete arguments[arguments.length - 1];
  return Handlebars.helpers["x"].apply(this, [Array.prototype.slice.call(
    arguments, 0).join(''), options]) ? options.fn(this) : options.inverse(
    this);
});



/*
More goodies since you're reading this gist.
*/

// say you have some utility object with helpful functions which you want to use inside of your handlebars templates

util = {

  // a helper to safely access object properties, think ot as a lite xpath accessor
  // usage:
  // var greeting = util.prop( { a: { b: { c: { d: 'hi'} } } }, 'a.b.c.d');
  // greeting -> 'hi'

  // [IMPORTANT] THIS .prop function is REQUIRED if you want to use the handlebars helpers below,
  // if you decide to move it somewhere else, update the helpers below accordingly
  prop: function() {
    if (typeof props == 'string') {
      props = props.split('.');
    }
    if (!props || !props.length) {
      return obj;
    }
    if (!obj || !Object.prototype.hasOwnProperty.call(obj, props[0])) {
      return null;
    } else {
      var newObj = obj[props[0]];
      props.shift();
      return util.prop(newObj, props);
    }
  },

  // some more helpers .. just examples, none is required
  isNumber: function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  },
  daysInMonth: function(m, y) {
    y = y || (new Date).getFullYear();
    return /8|3|5|10/.test(m) ? 30 : m == 1 ? (!(y % 4) && y % 100) || !(y %
      400) ? 29 : 28 : 31;
  },
  uppercaseFirstLetter: function(str) {
    str || (str = '');
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  hasNumber: function(n) {
    return !isNaN(parseFloat(n));
  },
  truncate: function(str, len) {
    if (typeof str != 'string') return str;
    len = util.isNumber(len) ? len : 20;
    return str.length <= len ? str : str.substr(0, len - 3) + '...';
  }
};

// a helper to execute any util functions and get its return
// usage: {{u 'truncate' this.title 30}} to truncate the title
Handlebars.registerHelper('u', function() {
  var key = '';
  var args = Array.prototype.slice.call(arguments, 0);

  if (args.length) {
    key = args[0];
    // delete the util[functionName] as the first element in the array
    args.shift();
    // delete the options arguments passed by handlebars, which is the last argument
    args.pop();
  }
  if (util.hasOwnProperty(key)) {
    // notice the reference to util here
    return typeof util[key] == 'function' ?
      util[key].apply(util, args) :
      util[key];
  } else {
    log.error('util.' + key + ' is not a function nor a property');
  }
});

// a helper to execute any util function as an if helper,
// that util function should have a boolean return if you want to use this properly
// usage: {{uif 'isNumber' this.age}} {{this.age}} {{else}} this.dob {{/uif}}
Handlebars.registerHelper('uif', function() {
  var options = arguments[arguments.length - 1];
  return Handlebars.helpers['u'].apply(this, arguments) ? options.fn(this) :
    options.inverse(this);
});

// a helper to execute any global function or get global.property
// say you have some globally accessible metadata i.e
// window.meta = {account: {state: 'MA', foo: function() { .. }, isBar: function() {...} } }
// usage:
// {{g 'meta.account.state'}} to print the state

// or will execute a function
// {{g 'meta.account.foo'}} to print whatever foo returns
Handlebars.registerHelper('g', function() {
  var path, value;
  if (arguments.length) {
    path = arguments[0];
    delete arguments[0];

    // delete the options arguments passed by handlebars
    delete arguments[arguments.length - 1];
  }

  // notice the util.prop is required here
  value = util.prop(window, path);
  if (typeof value != 'undefined' && value !== null) {
    return typeof value == 'function' ?
      value.apply({}, arguments) :
      value;
  } else {
    log.warn('window.' + path + ' is not a function nor a property');
  }
});

// global if
// usage:
// {{gif 'meta.account.isBar'}} // to execute isBar() and behave based on its truthy or not return
// or just check if a property is truthy or not
// {{gif 'meta.account.state'}} State is valid ! {{/gif}}
Handlebars.registerHelper('gif', function() {
  var options = arguments[arguments.length - 1];
  return Handlebars.helpers['g'].apply(this, arguments) ? options.fn(this) :
    options.inverse(this);
});

// just an {{#each}} warpper to iterate over a global array,
// usage say you have: window.meta = { data: { countries: [ {name: 'US', code: 1}, {name: 'UK', code: '44'} ... ] } }
// {{geach 'meta.data.countries'}} {{this.code}} {{/geach}}

Handlebars.registerHelper('geach', function(path, options) {
  var value = util.prop(window, arguments[0]);
  if (!_.isArray(value))
    value = [];
  return Handlebars.helpers['each'].apply(this, [value, options]);
});



/* Scandiweb test application */

$(window).on('hashchange', function() {
  render(decodeURI(window.location.hash));
});

$(window).trigger('hashchange');

$.validator.addMethod("regx", function(value, element, regexpr) {
  return regexpr.test(value);
}, "Please enter a valid format.");

$.validator.addMethod("skuValidation", function(value, element) {
  var isSuccess = true;
  $.ajax({
    url: "/products/findByField",
    data: {
      'field': 'sku',
      'value': value
    },
    method: "POST",
    async: false,
    success: function(data) {
      console.log(data);
      if (data !== null && typeof data === 'object') {
        isSuccess = false;

      }
    }
  });
  return isSuccess;
}, "Product with this SKU number already exist.");


var typeSelect = $('select[name=type]');
typeSelect.on('change', function(e) {

  templateLoader.register({
    key: "attribute",
    path: "/templates/attribute.html"
  });
  var attributeTemplateCompiled = Handlebars.compile(templateLoader.html(
    "attribute"));

  $.ajax({
    type: 'POST',
    data: {
      'type_id': typeSelect.val()
    },
    url: '/types/attributesByTypeId',
    success: function(data) {
      $('.type-group').empty();
      $.each(data, function(key, attribute) {

        $('.type-group').append(attributeTemplateCompiled(
          attribute));

        $('[name=attribute' + attribute.id + ']').rules('add', {
          required: true,
          regx: new RegExp(attribute.regex)
        });
      });
    }
  });
});

$("#add-product-form").validate({
  rules: {
    sku: {
      required: true,
      regx: /^[0-9A-Z]{5,20}$/,
      skuValidation: true
    },
    name: {
      required: true,
      minlength: 3,
      maxlength: 20,
      regx: /^[\w+\s+]+$/
    },
    price: {
      required: true,
      regx: /^([1-9][0-9]*|0)(\.[0-9]{2})?$/
    }
  }
});

var addProductPageBtn = $('#add-product-form input[type=button] ');
addProductPageBtn.on('click', function(e) {

  var postVars = {};
  $.each($('#add-product-form').serializeArray(), function(_, kv) {
    postVars[kv.name] = kv.value;
  });

  var attributes = $('.attributes').map(function() {
    return {
      attribute_id: $(this).data('id'),
      value: $(this).val()
    }
  }).get();

  newProduct = {
    'name': postVars['name'],
    'sku': postVars['sku'],
    'price': postVars['price'],
    'type': typeSelect.val(),
    'attributes': attributes
  };

  if ($("#add-product-form").valid()) {
    $.ajax({
      type: 'POST',
      url: '/products/add',
      data: newProduct,
      success: function(msg) {
        $('#add-product-form')[0].reset();
        window.location.hash = '#';
      }
    });
  }

});

var batchAction = $('#batchActions');

batchAction.on('click', function(e) {
  postVars = [];
  $.each($('.product-list-form').serializeArray(), function(_, kv) {
    postVars.push(kv.value)
  });

  var action = $(".batchAction option:selected").val();

  $('.loader').show();

  switch (action) {
    case 'delete':
      $.ajax({
        method: "POST",
        url: '/products/batchDelete',
        data: {
          'ids': postVars
        },
        success: function(msg) {
          $('.batchAction').prop('selectedIndex', 0);
          $('products-list').empty();
          $(window).trigger('hashchange');
        }
      });
      break;
  }

});


function render(url) {

  var temp = url.split('/')[0];

  $('.page').hide();

  var map = {
    '': function() {
      renderProductsPage();
    },

    '#addProduct': function() {
      renderAddProductPage();
    },
  };
  if (map[temp]) {
    map[temp]();
  } else {
    renderErrorPage();
  }

}

function generateAllProductsHTML(data) {
  var list = $('.all-products .products-list');
  templateLoader.register({
    key: "list",
    path: "/templates/list.html"
  });
  var listTemplateCompiled = Handlebars.compile(templateLoader.html("list"));
  var html = listTemplateCompiled(data);
  list.html(html);
  $('.loader').hide();
}

function renderProductsPage() {
  var page = $('.all-products');
  $('.loader').show();
  page.show("fast", function() {

    $.getJSON("/products/all", function(data) {
      generateAllProductsHTML(data);
    });

  });
}

function renderAddProductPage() {
  var page = $('.add-product');

  $('input[name=sku]').keyup(function() {
    $(this).val($(this).val().toUpperCase());
  });

  page.show("slow", function() {

    $.ajax({
      type: 'GET',
      url: '/types/all',
      success: function(data) {
        $('select[name=type').empty();
        $.each(data, function(key, type) {
          $('select[name=type').append($("<option></option>").attr(
            "value", type.id).text(type.name));
        });

      }
    });

    $("select[name=type]").trigger("change");

  });
}

function renderErrorPage() {
  var page = $('.error-page');
  page.addClass('visible');
}

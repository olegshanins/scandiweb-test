# Scandiweb test application from Olegs Hanins

Install php dependencies.
```php composer.phar install```

Run migrations (creates tables and insert data)
```php vendor/bin/phinx migrate -c config-phinx.php```

Run php build in server
```php -S localhost:8000 router.php```

Chekout my application
<http://localhost:8000/app.html>

Install node dependencies. Node and npm package manager should be installed.
```npm install```

Make a build (js, css)
```node ./node_modules/webpack/bin/webpack.js```

# I used
Development OS - Linux Fedora

Inspired by this guy took couple things from him. Migrations and Eloquent.

<https://github.com/sukonovs/scandiweb-test>

<https://siipo.la/blog/how-to-use-eloquent-orm-migrations-outside-laravel>

<http://docs.phinx.org/en/latest/migrations.html>

<https://laravel.com/docs/5.6/eloquent>

As example for jquery single page skeleton
<https://tutorialzine.com/2015/02/single-page-app-without-a-framework>

Bootstrap template
<https://blackrockdigital.github.io/startbootstrap-4-col-portfolio/>

As router library for rest api
<https://packagist.org/packages/klein/klein>

Backend validation library
<https://packagist.org/packages/vlucas/valitron>

Frontend validation library
<https://www.npmjs.com/package/jquery-validation>

For javascript templating + plugin
<https://www.npmjs.com/package/handlebars> + <https://gist.github.com/akhoury/9118682>

<?php
/**
 * This file helps to handle very first request to php built in server
 * I would say it something similar to .htaccess from apache
 *
 *
 *
 * Check if any static file is requested
 * if not then api.php will handle request.
 */
if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js|html)$/', $_SERVER["REQUEST_URI"])) {
    return false;
} else {
    include __DIR__ . '/api.php';
}

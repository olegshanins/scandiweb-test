<?php
namespace App\ModelRepositories;

use App\Models\Product;
use App\Models\ProductsAttributesValue;

/**
 * Repository class helps to work with models.
 * Construct complex arrays what may includes data from different models.
 * Also it inserts and removes data from database
 */
class ProductRepository
{
    /**
     * Get all products with their attributes
     * @return [array]
     */
    public function all()
    {
        $products = [];

        $productResult = Product::all();
        foreach ($productResult as $product) {
            $attributes = [];
            foreach ($product->attributesValues as $attributesValue) {
                $attributes[] = [
                'type' => $attributesValue->attribute->types->first()->name,
                'attribute'=> $attributesValue->attribute->name,
                'value' => $attributesValue->value,
                'measurement' => $attributesValue->attribute->measurement
              ];
            }

            $products[] = [
              'id' => $product->id,
              'name' => $product->name,
              'sku' => $product->sku,
              'price' => $product->price,
              'attributes' => $attributes
          ];
        }

        return $products;
    }

    /**
     * Create product with attributes
     * @param  [array] $data
     * @return [object]
     */
    public function create($data)
    {
        $product = Product::create(['name' => $data['name'], 'sku' => $data['sku'], 'price' => $data['price']]);

        $attributes = [];
        foreach ($data['attributes'] as $attribute) {
            array_push($attributes, new ProductsAttributesValue(['attribute_id' => $attribute['attribute_id'], 'value' => $attribute['value']]));
        }

        $product->attributesValues()->saveMany($attributes);

        return $product;
    }

    /**
     * Delete products by IDs
     * @param  [array] $ids
     * @return [int]
     */
    public function deleteByIds($ids)
    {
        $deletedRows = Product::whereIn('id', $ids)->delete();
        if ($deletedRows) {
            ProductsAttributesValue::whereIn('product_id', $ids)->delete();
        }

        return $deletedRows;
    }
}

<?php
namespace App\ModelRepositories;

use App\Models\Type;

/**
 * Repository class helps to work with models.
 * Construct complex arrays what may includes data from differen models.
 * Also it inserts and removes data from database
 */
class TypeRepository
{
    /**
     * Get attributes by type
     * @param  [int] $id
     * @return [array]
     */
    public function attributesByTypeId($id)
    {
        $attributes = [];

        $type = Type::find($id);

        $attributeResult = $type->attributes()->get();

        foreach ($attributeResult as $attribute) {
            $attributes[] = [
              'id' => $attribute->id,
              'name' => $attribute->name,
              'regex' => $attribute->regex,
              'helper' => $attribute->helper,
              'measurement' => $attribute->measurement
          ];
        }

        return  $attributes;
    }
}

<?php
namespace App;

/**
 * Config class keeps all configuration for the application
 */
class Config
{
    /**
     * Set $DB_DRIVER to required database type
     * sqlite, mysql etc...
     *
     * If you select other then sqlite
     * you may need to fill in other configuration
     */
    public static $DB_DRIVER  = 'sqlite';

    public static $DB_HOST  = 'localhost';

    public static $DB_PORT  = '3306';

    public static $DB_NAME  = '';

    public static $DB_USERNAME  = '';

    public static $DB_PASSWORD  = '';

    public static $DB_CHARSET  = 'utf8';

    public static $DB_COLLATION  = 'utf8_unicode_ci';

    public static $DB_TABLE_PREFIX  = '';
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Attribute model
 */
class Attribute extends Model
{
    /**
     * Disable auto fields like created_at, updated_at
     */
    public $timestamps = false;

    /**
     * Relation with Products attributes
     */
    public function values()
    {
        return $this->hasMany('App\Models\ProductsAttributesValue');
    }

    /**
     * Relation with type Model
     */
    public function types()
    {
        return $this->belongsToMany('App\Models\Type');
    }
}

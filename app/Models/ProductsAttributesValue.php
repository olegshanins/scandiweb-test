<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * ProductsAttributesValue model
 */
class ProductsAttributesValue extends Model
{
    /**
     * Disable auto fields like created_at, updated_at
     */
    public $timestamps = false;

    protected $fillable = ['attribute_id', 'value'];

    /**
     * Relation to Attribute
     */
    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute');
    }
}

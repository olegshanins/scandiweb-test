<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Product model
 */
class Product extends Model
{
    /**
     * Disable auto fields like created_at, updated_at
     */
    public $timestamps = false;

    protected $fillable = ['name', 'sku', 'price', 'type'];

    /**
     * Relation with attribute values
     */
    public function attributesValues()
    {
        return $this->hasMany('App\Models\ProductsAttributesValue');
    }
}

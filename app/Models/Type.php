<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * Disable auto fields like created_at, updated_at
     */
    public $timestamps = false;

    /**
     * Relation to Attribute
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Models\Attribute');
    }
}

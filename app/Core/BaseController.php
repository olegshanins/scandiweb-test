<?php
namespace App\Core;

use Sabre\HTTP;

/**
 * Initialize request, reposnse attributes.
 * Makes sure core constructor called and all core things avalible in application.
 * Includes some useful methods to return content with correct header.
 */
class BaseController extends Core
{
    public $request;

    private $response;

    public function __construct()
    {
        /**
         * Get current request and its data
         */
        $this->request = HTTP\Sapi::getRequest();

        /**
         * Helps to generate correct reponse
         */
        $this->response = new HTTP\Response();

        /**
         * Corre consructor called
         */
        parent::__construct();
    }

    /**
     * Method to return reponse
     * @param  string  $body
     * @param  integer $statusCode
     * @param  array   $headers
     */
    public function response($body = '', $statusCode = 200, $headers = ['Content-Type' => 'text/html'])
    {
        $this->response->setStatus($statusCode);

        foreach ($headers as $header => $value) {
            $this->response->setHeader($header, $value);
        }

        $this->response->setBody($body);

        HTTP\Sapi::sendResponse($this->response);
    }

    /**
     * Method to return json reponse
     * @param  string  $body
     * @param  integer $statusCode
     */
    public function responseJson($body = '', $statusCode = 200)
    {
        $this->response(json_encode($body), $statusCode, ['Content-Type' => 'application/json']);
    }
}

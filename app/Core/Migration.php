<?php
namespace App\Core;

use App\Core\Core;
use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;

class Migration extends AbstractMigration
{
    /** @var \Illuminate\Database\Capsule\Manager $capsule */
    public $capsule;

    /** @var \Illuminate\Database\Schema\Builder $capsule */
    public $schema;

    public function init()
    {
        $core = new Core();
        $this->capsule = $core->capsule;
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }
}

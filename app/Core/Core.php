<?php
namespace App\Core;

use App\Config;
use Illuminate\Database\Capsule\Manager;

/**
 * Initialize global things what my reuire application
 * As example database connection
 */
class Core
{
    public $capsule;

    public function __construct()
    {
        $this->initializeORM();
    }

    protected function initializeORM()
    {
        $this->capsule = new Manager();
        $this->capsule->addConnection([
        'driver' => Config::$DB_DRIVER,
        'database' => ((Config::$DB_DRIVER=='sqlite')?__DIR__ . '/../../db/phpsqlite.db':Config::$DB_NAME),
        'host'      => Config::$DB_HOST,
        'port'      => Config::$DB_PORT,
        'username'  => Config::$DB_USERNAME,
        'password'  => Config::$DB_PASSWORD,
        'charset'   => Config::$DB_CHARSET,
        'collation' => Config::$DB_COLLATION,
        'prefix' => Config::$DB_TABLE_PREFIX
    ]);
        $this->capsule->bootEloquent();
    }
}

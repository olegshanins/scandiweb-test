<?php
namespace App;

use App\Core\BaseController;
use App\ModelRepositories\ProductRepository;
use App\ModelRepositories\TypeRepository;
use App\Models\Product;
use Valitron\Validator;

/**
 * ProductController reponsible for all actions with products
 */
class ProductController extends BaseController
{
    /**
     * Gel all products
     * @return [json]
     */
    public function list()
    {
        $products = ProductRepository::all();
        $this->responseJson($products);
    }

    /**
     * Get one product by field. id, nam, sku, price
     * @return [json]
     */
    public function product()
    {
        $postData = $this->request->getPostData();
        $product = Product::where($postData['field'], '=', $postData['value'])->first();
        $this->responseJson($product);
    }

    /**
     * Add a product from POST data
     * @return [json]
     */
    public function add()
    {
        $postData = $this->request->getPostData();

        $v = new Validator($postData);

        $attributesRequired = [];

        $attributesToValidate = TypeRepository::attributesByTypeId((int)$postData['type']);

        foreach ($attributesToValidate as $attribute) {
            /**
             * Set product attributes to be required for validation
             */
            $attributesRequired = array_push($attributesRequired, 'attribute' . $attribute['id']);

            /**
             * Set regular expressions for attributes
             */
            $v->rule('regex', 'attribute' . $attribute['id'], '/'.$attribute['regex'].'/');
        }

        /**
         * Add validation to product fields.
         */
        $v->rule('required', array_merge(['name', 'sku', 'price', 'type'], $attributesRequired));
        $v->rule('regex', 'name', '/^[\w+\s+]+$/');
        $v->rule('lengthMin', 'name', 3);
        $v->rule('lengthMax', 'name', 20);
        $v->rule('regex', 'sku', '/^[0-9A-Z]{5,20}$/');
        $v->rule('regex', 'price', '/^([1-9][0-9]*|0)(\.[0-9]{2})?$/');

        $product = null;
        if ($v->validate()) {
            $product = ProductRepository::create($postData);
        }

        /**
         * Check if product added and return correct HTTP code
         */
        if ($product) {
            $statusCode = 201;
        } else {
            $statusCode = 200;
        }

        $this->responseJson($products, $statusCode);
    }

    /**
     * Remove products in a batch
     * @return [json]
     */
    public function batchDelete()
    {
        $postData = $this->request->getPostData();

        /**
         * Cast INT type to all array elements for security reason
         * @var array
         */
        $ids = array_map('intval', $postData['ids']);

        $removedCount = ProductRepository::deleteByIds($ids);

        /**
         * Check if any products reoved and return correct HTTP code
         */
        if ($removedCount) {
            $statusCode = 200;
        } else {
            $statusCode = 204;
        }

        $this->responseJson($removedCount, $statusCode);
    }
}

<?php
namespace App;

use App\Core\BaseController;
use App\ModelRepositories\TypeRepository;
use App\Models\Type;

/**
 * TypeController reponsible for all actions with types and its attributes
 */
class TypeController extends BaseController
{
    /**
     * Get all types
     * @return [json]
     */
    public function list()
    {
        $types = Type::all();
        $this->responseJson($types);
    }

    /**
     * Get all attributes for type by ID
     * @return [json]
     */
    public function attributes()
    {
        $postData = $this->request->getPostData();
        $attributes = TypeRepository::attributesByTypeId((int)$postData['type_id']);
        $this->responseJson($attributes);
    }
}
